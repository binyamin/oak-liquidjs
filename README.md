# Oak LiquidJS

Middleware for [Oak](https://github.com/oakserver/oak), to render files with the
[LiquidJS](https://liquidjs.com/) templating-engine.

Compatible with Deno Deploy.

## Usage

The module's default export is a function which returns middleware.

Any data stored under `app.state.globals` is passed to templates as global data.

```js
// Import the middleware
import views from 'https://denopkg.dev/gl/binyamin/oak-liquidjs/mod.ts';

// Initialize oak
const app = new oak.Application();

// Define global data under `app.state.globals`.
app.state.globals = {
	title: 'My Awesome Website',
};

// Initialize the middleware with the folder containing your
// templates.
app.use(views('views'));
```

### Render Templates

Use the `context.render` method

```js
// Initialize an oak router
const router = new oak.Router();

router.get(async (ctx, next) => {
	await ctx.render('index.html', {
		// Add data just for this template. (optional)
	});
	await next();
});
```

Templates may use frontmatter. To use another file as a layout, set the `layout`
front-matter key to its relative path. The template's contents will then be
available under [a _block_](https://liquidjs.com/tags/layout.html) named
"content".

## Contributing

All input is welcome; feel free to
[open an issue](https://gitlab.com/binyamin/oak-liquidjs/-/issues/new). Please
remember to be a [mensch](https://www.merriam-webster.com/dictionary/mensch). If
you want to program, you can browse
[the issue list](https://gitlab.com/binyamin/oak-liquidjs/-/issues).

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/oak-liquidjs/-/blob/main/LICENSE).
Copyright 2023 Binyamin Aron Green.

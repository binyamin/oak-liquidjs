# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project tries to adhere to
[Semantic Versioning (SemVer)](https://semver.org/spec/v2.0.0.html).

<!--
	**Added** for new features.
	**Changed** for changes in existing functionality.
	**Deprecated** for soon-to-be removed features.
	**Removed** for now removed features.
	**Fixed** for any bug fixes.
	**Security** in case of vulnerabilities.
-->

## [0.3.0] - 2023-04-03

### Changed

- Updated dependencies: oak, liquidjs, deno-std, template-engine

## [0.2.0] - 2022-12-29

### Added

- Export the `LiquidError` class, for identifying and handling LiquidJS-related
  errors.
- Add helper function `formatLiquidError`, for displaying human-friendly error
  messages for LiquidJS errors. Shows a stack-trace of the templates. It also
  prints a snippet of the problematic template, with syntax-highlighting.
- Add helper function `parseLiquidError`, for parsing a LiquidJS error

### Changed

- Updated liquidjs and deno-std
- If an error is thrown while rendering a template, it is rethrown as-is. The
  only exception is that, when a template does not exist, we wrap it in an HTTP
  error (500 status code). Previously, all errors were wrapped in an HTTP error.

## [0.1.1] - 2022-12-12

### Fixed

- Read layout keys from `app.state.globals`.
- Don't render layouts recursively

## [0.1.0] - 2022-12-12

### Added

- Render templates
- Use YAML front-matter
- Render `title` front-matter key using liquid
- Use `layout` front-matter key as shortcut for "layout" tag

[0.1.0]: https://gitlab.com/binyamin/oak-liquidjs/-/commits/v0.1.0
[0.1.1]: https://gitlab.com/binyamin/oak-liquidjs/-/commits/v0.1.1
[0.2.0]: https://gitlab.com/binyamin/oak-liquidjs/-/commits/v0.2.0
[0.3.0]: https://gitlab.com/binyamin/oak-liquidjs/-/commits/v0.3.0

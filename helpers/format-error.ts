import { colors, liquid, path } from './deps.ts';
import { highlight } from './terminal-highlight.ts';

export type ErrorInfo = {
	message: string;
	stack: {
		file: string | undefined;
		line: number;
		column: number;
		get input(): string;
		context: {
			token: liquid.Token;
			start: number;
			end: number;
			get text(): string;
		};
	}[];
	get cause(): liquid.LiquidError;
};

function sentenceCase(str: string): string {
	return str[0].toUpperCase() + str.slice(1);
}

/**
 * Compare the length of two strings.
 * @returns {-1|0|1} If `a` is longer, returns `1`. If it
 * is equal, return `0`. If it is shorter, returns `-1`.
 */
function isLonger(a: string, b: string): -1 | 0 | 1 {
	let i = 0;

	while (a[i] !== undefined) {
		if (b[i] === undefined) return 1;

		i++;
	}

	// If we're here, `a[i]` is undefined.

	// If `b[i]` is also undefined, they are of equal length
	if (b[i] === undefined) return 0;

	// Else, `a` is shorter.
	return -1;
}

function toSourceCode(frame: ErrorInfo['stack'][number]) {
	const lines = highlight(frame.input).split(/\r?\n/);

	// Both `start` and `end` are zero-indexed. For
	// `start`, we subtract 3 instead of 2, since `frame.line`
	// is one-indexed.
	const start = Math.max(frame.line - 3, 0);
	const end = Math.min(frame.line + 2, lines.length);

	// Note: Since `end` is the higher number, it has the
	// most characters.
	const maxWidth = String(end).length;

	return lines
		.slice(start, end)
		.map((line, index) => {
			const indent = ' '.repeat(6);

			const n = start + 1 + index;
			let gutter = (' ' + n).slice(-maxWidth);

			if (n === frame.line) {
				gutter = colors.bold(colors.white(gutter));
				line += '\n' + indent + gutter.replace(/\d/g, ' ') + ' | ' +
					line.slice(0, frame.column - 1).replace(/[^\t]/g, ' ') + '^';
			} else {
				gutter = colors.gray(gutter);
			}

			// Replace tabs with spaces
			line = line.replaceAll('\t', ' '.repeat(4));

			return indent + gutter + ' | ' + line;
		})
		.join('\n');
}

/**
 * Given a {@link liquid.LiquidError LiquidError}, returns
 * an object with the error message and a stack-trace of
 * the template files.
 */
export function parseLiquidError(error: liquid.LiquidError): ErrorInfo {
	const i = error.message.search(/, (file|line):/);
	const msg = error.message.slice(0, i);

	const files = new Map<string | undefined, string>();

	const stack: ErrorInfo['stack'] = [];

	let cause = error;

	while (cause && cause instanceof liquid.LiquidError) {
		const token: liquid.Token = cause['token'];

		// Update input, if necessary
		const prevInput = files.get(token.file);

		if (!prevInput || isLonger(token.input, prevInput) === 1) {
			files.set(token.file, token.input);
		}

		const frame = {
			line: -1,
			column: -1,
			file: token.file,
			get input() {
				return files.get(token.file)!;
			},
			context: {
				token,
				start: -1,
				end: -1,
				get text() {
					return this.token.getText();
				},
			},
		};

		const prev = stack.at(-1);

		// Position is relative to the token's `input`
		const position = token.getPosition() as [line: number, column: number];

		if (!prev) {
			frame.context.start = token.begin;
			frame.context.end = token.end;

			frame.line = position[0];
			frame.column = position[1];
		} else {
			// Chars before the token starts
			const before = prev.context.text.split(token.input, 2)[0];

			// Offset of current line, relative to the previous frame
			const lineOffset = before.split(/\r?\n/).length - 1;

			frame.line = prev.line + lineOffset + position[0] - 1;
			frame.column = position[1];
			if (position[0] === 1) {
				frame.column += prev.column - 1 + before.length;
			}

			// The current token's offset in the file, in characters.
			// Based on the current & previous frames.
			const charOffset = prev.context.start + before.length;

			frame.context.start = charOffset + token.begin;
			frame.context.end = charOffset + token.end;
		}

		stack.push(frame);

		cause = cause['originalError'];
	}

	stack.reverse();

	return {
		get cause() {
			return error;
		},
		message: msg,
		stack,
	};
}

/**
 * Given a {@link liquid.LiquidError LiquidError}, returns
 * a pretty error message with stack-trace of templates, and
 * the code where the error occured.
 */
export function formatLiquidError(error: liquid.LiquidError): string {
	const info = parseLiquidError(error);
	const lines: string[] = [];

	lines.push(sentenceCase(info.message));

	const prefix = ' '.repeat(4) + 'at ';

	let i = 0;
	for (const item of info.stack) {
		let line = prefix;

		if (item.file) {
			line += colors.cyan(
				'./' + path.relative('.', item.file),
			);
		} else {
			line += colors.dim('unknown');
		}

		line += ':' + colors.yellow(item.line.toString());
		line += ':' + colors.yellow(item.column.toString());

		lines.push(line);
		if (i === 0) {
			lines.push(toSourceCode(item));
		}
		i++;
	}

	return lines.join('\n');
}

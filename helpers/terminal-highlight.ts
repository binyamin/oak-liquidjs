import { colors } from './deps.ts';
import {
	Lexer,
	type Token,
	tokenName,
} from 'https://pkg.binyam.in/x/templating-engine@77403464/src/lexer.ts';

type tokenNames = ReturnType<typeof tokenName> | 'function';
const token_colors: Record<
	tokenNames,
	((str: string) => string)
> = {
	number: (s) => colors.yellow(s),
	string: (s) => colors.green(s),
	identifier: (s) => colors.red(s),
	symbol: (s) => s,
	tag: (s) => s,
	raw: (s) => s,
	keyword: (s) => colors.magenta(s),
	function: (s) => colors.cyan(s),
};

export function highlight(input: string) {
	const lexer = new Lexer(input);

	let code = '';
	const tokens: Token[] = [];

	let end = 0;
	let start = 0;

	for (const t of lexer.iterate()) {
		const p = tokens.at(-1);
		start = end;
		end = t.location.offset!;

		if (tokenName(t) === 'string') end--;

		if (p) {
			let name: tokenNames = tokenName(p);

			if (name === 'keyword' && /true|false|null/i.test(p.value)) {
				name = 'number';
			}

			if (name === 'identifier') {
				const pp = tokens.at(-2);
				if (pp?.value === '{%') {
					name = 'keyword';
				}

				if (pp?.value === '|') {
					name = 'function';
				}
			}

			code += token_colors[name](input.slice(start, end));
		}

		tokens.push(t);
	}

	const p = tokens.at(-1)!;

	code += token_colors[tokenName(p)](input.slice(end));

	return code;
}

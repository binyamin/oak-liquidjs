import { deepMerge, frontmatter, liquid, path } from './deps.ts';

function baseData(input: string): {
	layout?: string;
	title?: string;
	contents: string;
	[x: string]: unknown;
} {
	if (frontmatter.test(input)) {
		const matter = frontmatter.extract<{
			layout?: string;
			title?: string;
		}>(input);

		return {
			...matter.attrs,
			contents: matter.body,
		};
	} else {
		return {
			contents: input,
		};
	}
}

abstract class Adapter {
	#root: string;

	constructor(root: string) {
		this.#root = root;
	}

	protected abstract renderString(
		input: string,
		filename?: string,
		data?: Record<string, unknown>,
	): Promise<string>;

	protected abstract renderLayout(
		layout: string,
		contents: string,
		options?: {
			data?: Record<string, unknown>;
			filename?: string;
		},
	): Promise<string>;

	async render(input: string, data?: Record<string, unknown>): Promise<string>;
	async render(
		input: string,
		filename?: string,
		data?: Record<string, unknown>,
	): Promise<string>;
	async render(
		input: string,
		filename?: string | Record<string, unknown>,
		data?: Record<string, unknown>,
	): Promise<string> {
		if (!filename) {
			// No filename, no data
			filename = undefined;
			data ??= {};
		} else if (typeof filename !== 'string') {
			// Just data
			data = filename;
			filename = undefined;
		} else {
			// Both data and string
			data ??= {};
		}

		const { contents, ...base } = baseData(input);

		data = deepMerge(data, base);

		if (base.title) {
			data.title = await this.renderString(base.title, undefined, data);
		}

		if (data.layout) {
			const layout = data.layout as string;
			delete data.layout;

			return await this.renderLayout(layout, contents, {
				data,
				filename,
			});
		}

		return this.renderString(contents, filename, data);
	}

	async renderFile(
		file: string,
		data: Record<string, unknown> = {},
	): Promise<string> {
		file = path.resolve(this.#root, file);

		const input = await Deno.readTextFile(file);

		return this.render(input, file, data);
	}
}

export class LiquidAdapter extends Adapter {
	#liquid: liquid.Liquid;

	constructor(root: string, options: Omit<liquid.LiquidOptions, 'root'> = {}) {
		root = path.resolve(root);
		super(root);

		this.#liquid = new liquid.Liquid(Object.assign({}, options, { root }));
	}

	get liquid() {
		return this.#liquid;
	}

	protected renderLayout(layout: string, contents: string, options: {
		data?: Record<string, unknown>;
		filename?: string | undefined;
	} = {}): Promise<string> {
		contents =
			`{% layout "${layout}" %}{% block content %}${contents}{% endblock content %}`;
		return this.render(contents, options.filename, options.data);
	}

	protected renderString(
		input: string,
		filename?: string,
		data?: Record<string, unknown>,
	): Promise<string> {
		const tmpl = this.#liquid.parse(input, filename);
		return this.#liquid.render(tmpl, data);
	}
}

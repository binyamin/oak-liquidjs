import { deepMerge, liquid, oak } from './deps.ts';
import { LiquidAdapter } from './adapter.ts';

export interface Options {
	filters?: Record<string, liquid.FilterImplOptions>;
	liquid?: Omit<liquid.LiquidOptions, 'root'>;
}

export const LiquidError = liquid.LiquidError;

/**
 * @param root Folder with templates/views
 */
export function views(root: string, options: Options = {}): oak.Middleware {
	const engine = new LiquidAdapter(root, options.liquid);

	if (options.filters) {
		for (const [name, fn] of Object.entries(options.filters)) {
			engine.liquid.registerFilter(name, fn);
		}
	}

	return (ctx, next) => {
		// deno-lint-ignore no-explicit-any
		ctx.render = async (file: string, data: Record<string, any> = {}) => {
			try {
				const contents = await engine.renderFile(
					file,
					deepMerge(ctx.state.globals, data),
				);

				ctx.response.body = contents;
				// TODO calculate based on file extension (foo.liquid vs. foo.xml.liquid);
				ctx.response.headers.set('Content-Type', 'text/html; charset=utf-8');
			} catch (error) {
				const status = oak.Status.InternalServerError;

				if (error instanceof Deno.errors.NotFound) {
					ctx.throw(
						status,
						'The server tried to render a non-existent file.',
						{
							cause: error,
						},
					);
				}

				throw error;
			}
		};

		return next();
	};
}

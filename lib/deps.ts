export { liquid, path } from '../deps.ts';
export * as frontmatter from 'https://deno.land/std@0.181.0/front_matter/yaml.ts';
export { deepMerge } from 'https://deno.land/std@0.181.0/collections/deep_merge.ts';

declare module 'https://deno.land/x/oak@v12.1.0/mod.ts' {
	interface Context {
		// deno-lint-ignore no-explicit-any
		render: (filename: string, data?: Record<string, any>) => Promise<void>;
	}
}

export * as oak from 'https://deno.land/x/oak@v12.1.0/mod.ts';

import {
	assert,
	assertEquals,
} from 'https://deno.land/std@0.181.0/testing/asserts.ts';
import { oak } from '../lib/deps.ts';
import { views } from '../lib/middleware.ts';

const ctx = oak.testing.createMockContext({
	path: '/file',
	state: {
		globals: {
			color: 'grey',
		},
	},
});

const next = oak.testing.createMockNext();

views('test/fixtures', {
	liquid: {
		relativeReference: true,
	},
})(ctx, next);

Deno.test('Plain', async () => {
	await ctx.render('plain.liquid');

	assert(typeof ctx.response.body === 'string');
	assertEquals(ctx.response.body.trim(), 'Speak, friend, and enter.');
});

Deno.test('With local data', async () => {
	await ctx.render('local-data.liquid', {
		name: 'Frodo',
	});

	assert(typeof ctx.response.body === 'string');
	assertEquals(ctx.response.body.trim(), 'Speak, Frodo, and enter.');
});

Deno.test('With global data', async (t) => {
	await t.step('Using globals', async () => {
		await ctx.render('global-data.liquid');

		assert(typeof ctx.response.body === 'string');
		assertEquals(ctx.response.body.trim(), 'Gandalf the grey');
	});

	await t.step('Override globals', async () => {
		await ctx.render('global-data.liquid', {
			color: 'white',
		});

		assert(typeof ctx.response.body === 'string');
		assertEquals(ctx.response.body.trim(), 'Gandalf the white');
	});
});

Deno.test('With front-matter', async () => {
	await ctx.render('front-matter.liquid');

	assert(typeof ctx.response.body === 'string');
	assertEquals(
		ctx.response.body.trim().replace(/\r?\n/, '\n'),
		'# In Mordor\n\nIn the land of Mordor, where the shadows lie.',
	);
});

Deno.test('With layout', async () => {
	await ctx.render('with-layout/file.liquid');

	assert(typeof ctx.response.body === 'string');
	assertEquals(
		ctx.response.body.trim(),
		'Gandalf: Fly, you fools!',
	);
});
